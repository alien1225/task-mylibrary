<?php


class UserController
{

    public function actionRemoteBookById ($id) {
        User::removeBookById($id);
    }

    public function actionBook() {
        if (isset($_POST['addBook'])) {
            $nameB = $_POST['nameBook'];
            $authorB = $_POST['authorBook'];
            $pagesB = $_POST['pagesBook'];

            $error = [];

            if (empty($nameB)) {
                $error[] = "Поле імя книги пусте";
            }else if (strlen($nameB) < 2) {
                $error[] = "Імя книги занадто коротке";
            }

            if (empty($authorB)) {
                $error[] = "Поле автор книги пусте";
            }else if (strlen($authorB) < 2) {
                $error[] = "Імя автора книги занадто коротке";
            }

            if (empty($pagesB)) {
                $error[] = "Поле кількість сторінок книги пусте";
            }else if ($pagesB < 1) {
                $error[] = "Некоректна кількість сторінок";
            }

            if (empty($error)) {
                User::addBook($nameB, $authorB, $pagesB);

            }
        }
        $books = User::getAllBook();
        require_once ROOT.'/views/testTask/bookMyRoom/allBook.php';
    }
}