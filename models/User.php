<?php
use RedBeanPHP\R as R;

class User
{
    public static function getAllBook () {
        $books = R::findAll('book');
        $numBooks = R::count('book');

        if ($numBooks < 1) {
            return false;
        }

        return $books;
    }

    public static function removeBookById (int $id) {
        $book = R::load('book', $id);

        R::trash($book);
        return true;
    }

    public static function addBook ($name, $author, $coutPage) {
        $book = R::dispense('book');
        $book->name = $name;
        $book->author = $author;
        $book->count = $coutPage;
        R::store($book);
    }

}