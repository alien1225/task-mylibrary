# My Library


### Configuration
  - Ubuntu 16.04;
  - php 7.2;
  - mysql;
  - Apache/2.4.41.
  
### Installation
1. Git
```sh
$ git clone https://alien1225@bitbucket.org/alien1225/task-mylibrary.git
```
2. In the project folder:
```sh
$ composer install
```
 *After that it will be installed ORM RedBean;*
 
 3. Config DB: /components/RedBean.php ;
4. Create `book` table in DB.

**Book table**

| id | name | author | count |
| ------ | ------ | ------ | ------ | 
| int(11) | varchar(128) | author(128) | int(11) |

**Link main page:** *user/all-book*