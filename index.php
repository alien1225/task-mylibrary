<?php

// FRONT CONTROLLER
session_start();

ini_set("display_errors", 1);
error_reporting(E_ALL);

define("ROOT", dirname(__FILE__));

require_once ROOT.'/components/Autoload.php';
require_once ROOT.'/vendor/autoload.php';
require_once ROOT.'/components/RedBean.php';

$router = new Router();
$router->run();
