<?php
use \RedBeanPHP\R as R;
$db_config = include ROOT.'/config/db_params.php';
R::setup("mysql:host=$db_config[host];dbname=$db_config[dbname]",
    $db_config['user'],
    $db_config['password']
);
R::freeze(false);
if (!R::testConnection()) {
    die('No db connection!');
}
