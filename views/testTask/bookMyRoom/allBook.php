<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Test task</title>
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="header">
                <h2 class="text-center">Мої Книги</h2>
            </div>
        </div>
        <div class="col-12">
            <div class="settings">
                <div class="card">
                    <div class="card-header">
                        Додати нову книгу
                    </div>
                    <div class="card-body">
                        <form method="post">
                            <div class="form-row">
                                <div class="col-4">
                                    <input type="text" value="<?= !empty($nameB) ? $nameB : ""?>" name="nameBook" class="form-control" placeholder="Назва книги">
                                </div>
                                <div class="col-4">
                                    <input type="text" value="<?= !empty($authorB) ? $authorB : ""?>" name="authorBook" class="form-control" placeholder="Автор">
                                </div>
                                <div class="col-2">
                                    <input type="text" value="<?= !empty($pagesB) ? $pagesB : ""?>" name="pagesBook" class="form-control" placeholder="Кількість сторінок">
                                </div>
                                <div class="col-2">
                                    <button type="submit" name="addBook" class="btn btn-success">Додати нову книгу</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

            <div class="message">
                <?php


                if (isset($_POST['addBook']) && empty($error)) {?>
                    <div class="alert alert-success" role="alert">
                        Книга успішно добавленна!
                    </div>
                <?php }else if (!empty($error)) {?>
                    <div class="alert alert-danger" role="alert">
                        Помилки
                        <ul>
                            <?php
                            foreach ($error as $value) {
                                echo '<li>'.$value.'</li>';
                            }

                            ?>
                        </ul>

                    </div>
                <?php } ?>

            </div>
        </div>


        <div class="col-12">
            <hr>
            <div class="body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">Назва книги</th>
                        <th scope="col">Автор</th>
                        <th scope="col">Кількість сторінок</th>
                        <th scope="col">Дія</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($books === false) {
                    ?>
                        <tr>
                            <th colspan="5" scope="row"><center>Книг в кімнаті не знайденно</center></th>
                        </tr>
                    <?php
                    }else {
                        foreach ($books as $book) {
                    ?>
                            <tr>
                                <th scope="row"><?=$book->id?></th>
                                <td><?=$book->name?></td>
                                <td><?=$book->author?></td>
                                <td><?=$book->count?></td>
                                <td><a href="/user/all-book/remove-<?=$book->id?>" class="btn btn-danger">X</a></td>
                            </tr>

                    <?php }
                    }?>


                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
